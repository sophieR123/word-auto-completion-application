# Word Auto Completion Application #
 
Coursework 2 for Data Structures and Algorithms Module at University of East Anglia 
 
### Coursework Description ###
 

* Read in a document of words, find the set of unique words to form a dictionary, count the occurrence of each word, then save the words and counts to file;
* define a trie data structure for storing a suffix tree and design and implement algorithms to manipulate the trie; and
* make a word completion system using the dictionary from part 1 and the data structure from part 2.
 
### Running ###
 
Clone the rpo add to a IDE with Java SDK set up. Run the files as required. 
 
### Grade Recieved ###
 
Percentage Grade : 86%
 
Grade : First
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dsacoursework2;

import java.io.File;
import java.io.PrintStream;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

/**
 *
 * @author 100021268
 */
public class AutoCompletion {
   
    private static final long STARTTIME = System.nanoTime();
    //Creating dictionary section 
    public static void main(String[] args) throws Exception {
        DictionaryMaker lotrDict=new DictionaryMaker();
        lotrDict.formDictionary("lotr.csv");
        lotrDict.saveToFile("lotrDictionary.txt");
        String scannerWord = "";
        Scanner scan = new Scanner(new File("lotrDictionary.txt"));
        ArrayList<String> lotrList = new ArrayList<String>();
        while (scan.hasNext()){
            for(char c : scan.next().toCharArray()){
                if(c=='[' || c==']'){
                    continue;
                }
                if(c!=','){
                    scannerWord = scannerWord + c;
                }
                if(c==','){
                    lotrList.add(scannerWord);
                    scannerWord = ""; 
                }
            }
        }
        scan.close();

        Trie lotrTrie=new Trie();
        int counter=2; 
        String temp = "";
        for(String lotrWord : lotrList){
            if(counter%2==0){
                temp = lotrWord;
            }
            else{
               for(int i = 0; i<Integer.parseInt(lotrWord); i++){
                   lotrTrie.add(temp);
               }
               temp = "";
            }
            counter++; 
        }
        
        Scanner scanQueries = new Scanner(new File("lotrQueries.csv"));
        
        PrintStream lotrWriteTo = new PrintStream(new File("lotrMatches.csv"));
        while(scanQueries.hasNextLine()){
            String prefix = scanQueries.nextLine();
            System.out.println("\nResults for prefix : " + prefix);
            Trie subTrie = lotrTrie.getSubTrie(prefix, lotrTrie.getRoot());
            List<String> queryWords = subTrie.getAllWords(subTrie.getRoot());
            List<Double> frequencyList = subTrie.getAllFrequencies(subTrie.getRoot());
            
            if(lotrTrie.contains(prefix, lotrTrie.getRoot())){
                queryWords.add(prefix);
                frequencyList.add(lotrTrie.getFrequency(subTrie.getRoot()));
            }
            
            double frequencyTotal = 0;
            for(double e : frequencyList){
                frequencyTotal += e;
            }
            
            int count = 0;
            for(double e : frequencyList){
                frequencyList.set(count, e/frequencyTotal);
                count++;
            }
            
            for(int i=0; i<queryWords.size(); i++){
                String word = queryWords.get(i);
                if(word!=prefix){
                    queryWords.set(i, (frequencyList.get(i) + ","+ prefix + word ));
                }
                else{
                    queryWords.set(i, (frequencyList.get(i) + ","+ word ));
                }
            }
            
            Collections.sort(queryWords);
            Collections.reverse(queryWords);
            DecimalFormat df = new DecimalFormat("#.####");
            
            lotrWriteTo.print(prefix + ",");
            
            String[] word;
            if(queryWords.size()>5){
                for(int i=0; i<5; i++){
                    word = queryWords.get(i).split(",");
                    if(Double.parseDouble(word[0])>0){
                        System.out.println(word[1] + " (probability " + df.format(Double.parseDouble(word[0])) + ")");
                        lotrWriteTo.print(word[1] + "," + df.format(Double.parseDouble(word[0])) + ",");
                    }
                }
            }
            
            else{
                for(int i=0; i<queryWords.size(); i++){
                    word = queryWords.get(i).split(",");
                    if(Double.parseDouble(word[0])>0){
                        System.out.println(word[1] + " (probability " + df.format(Double.parseDouble(word[0])) + ")");
                        lotrWriteTo.print(word[1] + "," + df.format(Double.parseDouble(word[0])) + ",");
                    }
                }
            }
            
            if(queryWords.isEmpty()){
                System.out.println(prefix + " does not exist in the lotr dictionary");
                lotrWriteTo.print("Prefix not found in the lotr dictionary");
            }
            
            lotrWriteTo.print("\n");
            
        }
        
    long endTime   = System.nanoTime();
    long totalTime = endTime - STARTTIME;
    float otherTotalTime = totalTime/1000000000f;
    System.out.println(otherTotalTime);
    }
}

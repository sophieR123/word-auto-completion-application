
package dsacoursework2;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Scanner;

/**
 *
 * @author ajb
 */
public class DictionaryMaker {
    
    ArrayList<String> dictionaryList = new ArrayList();
    
    public DictionaryMaker(){
    }
/**
 * Reads all the words in a comma separated text document into an Array
 * @param f 
 */   
    public static ArrayList<String> readWordsFromCSV(String file) throws FileNotFoundException {
        
        Scanner scan=new Scanner(new File(file));
        scan.useDelimiter(" |,");//specifies the two different delimiters, space and comma.
        ArrayList<String> words=new ArrayList<>();
        String str;
        while(scan.hasNext()){
            str=scan.next();
            str=str.trim();
            str=str.toLowerCase();
            words.add(str);
        }
        return words;
    }
    
    public static void saveCollectionToFile(Collection<?> c,String file) throws IOException {
        FileWriter fileWriter = new FileWriter(file);
        PrintWriter printWriter = new PrintWriter(fileWriter);
         for(Object w: c){
            printWriter.println(w.toString());
         }
        printWriter.close();
    }
     
    public void formDictionary(String fileToReadFrom) throws FileNotFoundException{
        HashMap dictionary = new HashMap();
        ArrayList<String> words= readWordsFromCSV(fileToReadFrom);
        for(String word:words){
            if(!(dictionary.containsKey(word))){
                 dictionary.put(word,1);
            }
            else{
                int i = (int) dictionary.get(word);
                i++;
                dictionary.replace(word,i);
            }
        }
        Iterator dictionaryIterator = dictionary.entrySet().iterator();
        while(dictionaryIterator.hasNext()) {
            Map.Entry me = (Map.Entry)dictionaryIterator.next();
            dictionaryList.add(me.getKey() + "," + me.getValue());
        }
        Collections.sort(dictionaryList);
    }
     
    public void saveToFile(String fileToSaveTo){
        Writer writer = null;
        try {
            writer = new BufferedWriter(new OutputStreamWriter(
                    new FileOutputStream(fileToSaveTo), "utf-8"));
            writer.write(dictionaryList.toString());
        } catch (IOException ex) {
            // Report
        } finally {
           try {writer.close();} catch (Exception ex) {/*ignore*/}
        }
    }
         
    //Question1 
    public static void main(String[] args) throws Exception {
        DictionaryMaker df=new DictionaryMaker();
        df.formDictionary("testDocument.txt");
        df.saveToFile("dictionarySave.txt");
    }
    
}
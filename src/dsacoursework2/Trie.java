/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dsacoursework2;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author 100021268, jxy13mmu
 */
public class Trie {
    
    TrieNode root = new TrieNode();
    String dFSOutput = "";
    String allWords = "";
    String prefix = "";
    List<String> list = new ArrayList();
    List<Double> dblList = new ArrayList();
    
    TrieNode nodeHere = new TrieNode();
    Trie(){}
    
    Trie(TrieNode root){
        this.root=root;
    }
    
    TrieNode getRoot(){
        return root; 
    }
    
    //question 2.1
    boolean add(String word){//must return true if add was successful
        int letterTracker=0;
        list.clear();//makes code more robust
        return root.setOffspring(word, letterTracker);
    }
  
    //question 2.2
    public boolean contains(String word, TrieNode previousNode){
        for(TrieNode currentNode : previousNode.getOffspring()){
            if(currentNode!=null){
                if(word.charAt(0)==currentNode.getLetter()){
                    if(word.length()==1){
                        return true;
                    }
                    word=word.substring(1);
                    return contains(word,currentNode);
                }
            }
        }
        return false;
    }
        
    //question 2.3
    String outputBreadthFirstSearch(){//needs prefix adding also 
        String breadthFirstSearch = "";  
        ArrayList<TrieNode> toVisit = new ArrayList();
        for(TrieNode t : root.getOffspring()){
            if(t!=null){
                toVisit.add(t);
                breadthFirstSearch = breadthFirstSearch + t.getLetter();
            }
        }
        while(toVisit.isEmpty()==false){
            TrieNode currentElement = toVisit.get(0);
            for(TrieNode t : currentElement.getOffspring()){
                if(t!=null){
                    toVisit.add(t);
                    breadthFirstSearch = breadthFirstSearch + t.getLetter();
                }
            }
            toVisit.remove(0);
        }
        return breadthFirstSearch;
    }
        
    //question 2.4
    String outputDepthFirstSearch(TrieNode previousNode){//needs prefix adding also 
        for(TrieNode currentNode : previousNode.getOffspring()){
            if(currentNode!=null){
                outputDepthFirstSearch(currentNode);
                dFSOutput = dFSOutput + currentNode.getLetter();
            }
          }
        return dFSOutput;
    }    

    //question 2.5
    Trie getSubTrie(String prefix, TrieNode previousNode){//needs prefix adding also \
        for(TrieNode currentNode : previousNode.getOffspring()){
            if(currentNode!=null){
                if(currentNode.getLetter()==prefix.charAt(0)){
                    if(prefix.length()>1){
                        prefix = prefix.substring(1);
                        return getSubTrie(prefix, currentNode);
                    }
                    else if(prefix.length()==1){
                        return new Trie(currentNode);
                    }
                }
            }
        }
        return new Trie();
    }
    
    //question 2.6
    List getAllWords(TrieNode previousNode){
        for(TrieNode currentNode : previousNode.getOffspring()){
            if(currentNode!=null){
                prefix = prefix + currentNode.getLetter();
                if(currentNode.getFlag()==true){
                    list.add(prefix);
                }
                if(currentNode.hasValidOffspring==true) {
                    getAllWords(currentNode);
                }
                prefix=prefix.substring(0,prefix.length()-1);
            }
          }
        return list;
    }  
    
    //helperMethods for Question 3
    List getAllFrequencies(TrieNode previousNode){
        for(TrieNode currentNode : previousNode.getOffspring()){
            if(currentNode!=null){
                if(currentNode.getFlag()==true){
                    dblList.add(currentNode.getFrequency());
                }
                if(currentNode.hasValidOffspring==true) {
                    getAllFrequencies(currentNode);
                }
            }
          }
        return dblList;
    }  
    
    double getFrequency(TrieNode node){
        return node.getFrequency();
    }
    
    //Question2 Outputs
    public static void main(String[] args) throws Exception {
        Trie trie=new Trie();
        
        trie.add("a");
        trie.add("at");
        trie.add("attack");
        trie.add("cheers");
        trie.add("cheese");
        trie.add("chat");
        trie.add("cat");
        trie.add("bat");
        System.out.println("Searching for cheese, is it present ? " + trie.contains("cheese", trie.getRoot()));
        System.out.println("Searching for BAT, is it present ? " + trie.contains("bat", trie.getRoot()));
        System.out.println("Searching for AT, is it present ? " + trie.contains("at", trie.getRoot()));
        System.out.println(trie.outputBreadthFirstSearch());
        System.out.println(trie.outputDepthFirstSearch( trie.getRoot()));
        Trie subTrie = trie.getSubTrie("ch", trie.getRoot());
        System.out.println("root node : " + subTrie.getRoot().getLetter());
        System.out.println("subtrie : "+ subTrie.outputBreadthFirstSearch());
        System.out.println("All words : " + trie.getAllWords(trie.getRoot()));
        System.out.println("All freaks : " + trie.getAllFrequencies(trie.getRoot()));
  
        trie.add("campus");
        trie.add("computing");
        trie.add("sunday");
        trie.add("march");
        trie.add("march");
        trie.add("hello");
        System.out.println("All words : " + trie.getAllWords(trie.getRoot()));
        System.out.println("All frequencies : " + trie.getAllFrequencies(trie.getRoot()));
  
    }
}

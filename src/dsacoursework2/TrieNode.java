/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dsacoursework2;

/**
 *
 * @author 100021268
 */
public class TrieNode {
    char letter;
    boolean flag = false;
    TrieNode[] offspring = new TrieNode[26];
    int depthOfTrie;
    int currentDepth;
    double frequencyCount; 
    boolean hasValidOffspring = false; 
    
    public TrieNode(char letterValue, int letterTracker){
        letter = letterValue;
        flag = false; 
        currentDepth = letterTracker;
        frequencyCount = 0;
        depthOfTrie = 1;
    }
    
    public TrieNode(){
        letter = ' ';
        flag = false; 
    }
    
    TrieNode[] getOffspring(){
            return offspring;
        //returns the TrieNode of the offspring 
    }  

    char getLetter(){
        return letter;
        //returns the letter of the currentNode
    }  

    boolean getFlag(){
        return flag;
    }
    
    int getDepthOfTree(){
        return depthOfTrie;
    }
    
    int getCurrentDepth(){
        return currentDepth;
    }
    
    int size(){
        int count=0;
        for(TrieNode t : offspring){
            if(t!=null){
                count++;
                count+=t.size();
            }
        }
        return count;
    }
    
    void setFlag(){
        flag = true;
    }
    
    void setFrequency(){
        frequencyCount++;
    }
    
    double getFrequency(){
        return frequencyCount;
    }
    
    boolean setOffspring(String word, int letterTracker){
        if(letterTracker==word.length()){
            setFlag();
            setFrequency();
            return true;
        }
        if(word.length()>depthOfTrie){
            depthOfTrie = word.length();
        }
        char currentChar = word.charAt(letterTracker);
        int position = (int) currentChar;
        position-=97; //shifts so in range of the array index\
        if(word.length()>1){
            if(offspring[position]==null){
                offspring[position] = new TrieNode(currentChar, letterTracker);
            }
        }
        else{
            if(offspring[position]==null){
                offspring[position] = new TrieNode(currentChar, letterTracker);
            }
        }
        letterTracker++;
        offspring[position].setOffspring(word,letterTracker);
        hasValidOffspring = true; 
        return true; //will never reach this but requires it as boolean type
    }
    
}
